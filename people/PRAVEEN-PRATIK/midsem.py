# ===================
# Mid Sem Exam
# ===================
# - Please copy the file called `midsem.py` into your **OWN** folder.
# - From now on, everything must be done in your own copy of the file.
# - Edit the file to solve the problem stated in it.
# - Save it, then commit your code and push to your fork of the project.
# - Make sure the tests are passing. If tests fail, you can look at the tests and figure out what you did wrong.
# - Submit a merge request to the Master branch of the class project.
# - Make sure that rebase issues do not exist or give rebase permissions to reviewer while creating the merge request
# - Once you are sure you are done and want to submit, simply mention @theSage21 in the comments of the merge request.
# - The timestamp on this comment shows when you finished your exam. You must finish within the exam time limit.

# NOTE: If at any time you have questions, please open an issue in the class project

# ===================
# Statement
# ===================

# You must write a function that returns a location in a game of tic-tac-toe
# your agent must try to win as many games as possible
# The board is a tuple of 3 strings. For example the starting position is:
#     ('   ',
#      '   ',
#      '   ')
# If your agent decides to put your symbol in coordinates (0, 0) the board would look like:

#     ('x  ',
#      '   ',
#      '   ')

# Your agent must return two coordinates whenever it is called
# for example in order to make the move shown above your function
# must be something like this:

# def agent(board, your_symbol):
#     return 0, 0


# ===================
# Scoring
# ===================

# The scoring is simple.
# For every game you win, you get 1 point
# For everything else you get 0 point (lose a game/end game due to invalid move)
# There are two test cases for this problem
# Each test case has a single opponent algorithm your agent must try to win against.
# You can see the jobs in the merge request to see how your submission is doing


def agent(board, your_symbol):
    ...  # TODO: Please complete the function
    print(board[7] + '|' + board[8] + '|' + board[9])
    print(board[4] + '|' + board[5] + '|' + board[6])
    print(board[1] + '|' + board[2] + '|' + board[3])

#Making test version of the board
test_board = ['#','X','O','X','O','X','O','X','O','X']
agent(test_board)

#Making function to take player as input and assign marker as x or o
def player_input():
    marker = ''
    while marker != 'x' and marker !='o':
        marker = input('playa 1, choose x or o: ')
    playa1 = marker
    if playa1 == 'x':
        playa2 = 'o'
    else:
        playa2 = 'x'
    return (playa1, playa2)

#Running the function 
player_input()playa 1, choose x or o: 
playa 1, choose x or o: 
playa 1, choose x or o: o

test_board

def place_marker(board, marker, position):
    board[position] = marker

test_board

#Running the place marker function using test parameters and display the modified board
place_marker(test_board,'$',8)
agent(test_board)

#Function that takes in a board and a mark (X or O) and check to see if that mark has won
def win_check(board, mark):
    return ((board[4] == board[5] == board[6] == mark) or
    (board[7] == board[8] == board[9] == mark) or
    (board[1] == board[2] == board[3] == mark) or
    (board[1] == board[4] == board[7] == mark) or
    (board[2] == board[5] == board[8] == mark) or
    (board[3] == board[6] == board[9] == mark) or
    (board[7] == board[5] == board[3] == mark) or
    (board[9] == board[5] == board[1] == mark))

#Running win check function against out test_board
agent(test_board)
win_check(test_board,'X')

#Function to randomly decide which player goes first
import random

def choose_first():
    flip = random.randint(0,1)
    if flip == 0:
        return 'Player 1'
    else:
        return 'Player 2'

#Function that returns a boolean indicating whether a space on the board is freely available
def space_check(board, position):
    return board[position] == ' '

#Function that checks if the board is full and returns a boolean value
def full_board_check(board):
    for i in range(1,10):
        if space_check(board, i):
            return False
    return True #board is full#

#Function that asks for a player’s next position
def player_choice(board):
    position = 0
    while position not in [1,2,3,4,5,6,7,8,9] or not 
space_check(board, position):
        position = int(input('Choose a position 1-9 '))
    
    return position

print('Welcome to Tic Tac Toe!')
while True:   
    # Set the game up here (board, who first, choose markers X,O)
    the_board = [' '] * 10
    playa1_marker, playa2_marker = player_input()
    
    turn = choose_first()
    print(turn + 'will go first')
    
    play_game = input('Ready to play? y or n')
    if play_game == 'y':
        game_on = True
    else:
        game_on = False
    
   
   # Play the game 
    while game_on:
        if turn == 'Player 1': #Player 1 Turn
            
            agent(the_board)#show the board
            position = player_choice(the_board)#choose position
            place_marker(the_board, playa1_marker, position)
            
            #check if they won 
            if win_check(the_board, playa1_marker):
                agent(the_board)
                print('Player 1 has won!!')
                game_on = False
            #check if its a tie
            else:
                if full_board_check(the_board):
                    agent(the_board)
                    print('tie game')
                    break
                else:
                    turn = 'Player 2' #no tie and no win  next player turn
        else:
             #show the board  # Player2's turn
            agent(the_board)
            position = player_choice(the_board)
            place_marker(the_board, playa2_marker, position)
            
            #check if they won 
            if win_check(the_board, playa2_marker):
                agent(the_board)
                print('Player 2 has won!!')
                game_on = False
            #check if its a tie
            else:
                if full_board_check(the_board):
                    agent(the_board)
                    print('tie game')
                    break
                else:
                    turn = 'Player 1' #no tie and no win  next 
player turn
            
            
#if not replay():
    if not replay():
        break
