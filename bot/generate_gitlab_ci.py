import os

ci_job = """
{jobname}:
    image: 'python:3.6'
    variables:
        NAME: '{name}'
    before_script:
        - pip install pipenv
        - echo $NAME
        - cp Pipfile "people/$NAME"
        - cp Pipfile.lock "people/$NAME"
        - cd "people/$NAME"
        - pipenv install --dev --deploy
        - touch dummy.py
    script:
        - for file in  *py;
          do
          pipenv run python $file;
          done
    stage: assignment
"""

print(
    """stages:
    - assignment
"""
)

for name in sorted(os.listdir("../people")):
    jobname = name.lower()
    print(ci_job.format(jobname=jobname, name=name))
